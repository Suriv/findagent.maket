// Core
import React from 'react';

// Instrument
import insuranceIco from "../theme/assets/icons/insurance.svg";
import wordpressIco from "../theme/assets/icons/wordpress.svg";
import favoriteIco from "../theme/assets/icons/favorite.svg";
import freeIco from "../theme/assets/icons/free.svg";
import informationIco from "../theme/assets/icons/information.svg";
import insuranceBg from "../theme/assets/homedopnav/insurance.jpg";
import wordpressBg from "../theme/assets/homedopnav/wordpress.jpg";
import agentBg from "../theme/assets/homedopnav/agentbest.jpg";
import serviceBg from "../theme/assets/homedopnav/servicefree.jpg";
import helpBg from "../theme/assets/homedopnav/help.jpg";

export default class Homedopnav extends React.Component {
    render() {
        return (
            <div className = 'homedopnav'>
                <ul className="homedopnav__list">
                    <li className="homedopnav__item">
                        <div className="homedopnav-title">
                            <span className="homedopnav-title__txt">Цифровая ипотека</span>
                            <img className="homedopnav-title__ico" src={insuranceIco} alt=""/>
                        </div>
                        <div className="homedopnav__item-bg" style={{backgroundImage: `url(${insuranceBg})`}}></div>
                        <a href=""></a>
                    </li>
                    <li className="homedopnav__item">
                        <div className="homedopnav-title">
                            <span className="homedopnav-title__txt">Блоги рынка</span>
                            <img className="homedopnav-title__ico" src={wordpressIco} alt=""/>
                        </div>
                        <div className="homedopnav__item-bg" style={{backgroundImage: `url(${wordpressBg})`}}></div>
                        <a href=""></a>
                    </li>
                    <li className="homedopnav__item">
                        <div className="homedopnav-title">
                            <span className="homedopnav-title__txt">Рейтинг агентов по недвижимости</span>
                            <img className="homedopnav-title__ico" src={favoriteIco} alt=""/>
                        </div>
                        <div className="homedopnav__item-bg" style={{backgroundImage: `url(${agentBg})`}}></div>
                        <a href="/agents" className="homedopnav__item-link"></a>
                    </li>
                    <li className="homedopnav__item">
                        <div className="homedopnav-title">
                            <span className="homedopnav-title__txt">Бесплантый сервис </span>
                            <img className="homedopnav-title__ico" src={freeIco} alt=""/>
                        </div>
                        <div className="homedopnav__item-bg" style={{backgroundImage: `url(${serviceBg})`}}></div>
                        <a href=""></a>
                    </li>
                    <li className="homedopnav__item">
                        <div className="homedopnav-title">
                            <span className="homedopnav-title__txt">Помощь в предпродажной подготовке</span>
                            <img className="homedopnav-title__ico" src={informationIco} alt=""/>
                        </div>
                        <div className="homedopnav__item-bg" style={{backgroundImage: `url(${helpBg})`}}></div>
                        <a href=""></a>
                    </li>
                </ul>
            </div>
        );
    }
}