// Core
import React from 'react';

// Instrument
import icoView from '../theme/assets/icons/view.png';

export default (props) => {
    return  (
        <li className="blog__el">
            <div className="blog__el-in">
                <div className="blog__img-cont">
                    <div className="blog__img" style={{backgroundImage: `url(${ props.imgBlog})`}} />
                </div>
                <div className="blog__text">
                    <div className="blog__text-top">
                        <div className="blog__view">
                            <div className="blog__view-ico-wrap">
                                <img className="blog__view-ico" src={ icoView } />
                            </div>
                            <div className="blog__view-txt">
                                { props.viewBlog }
                            </div>
                        </div>
                        <div className="blog__date">{ props.dateBlog }</div>
                    </div>
                    <div className="blog__title">{ props.titleBlog }</div>
                </div>
            </div>
        </li>
    );
};