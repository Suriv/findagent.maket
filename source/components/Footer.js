// Core
import React from 'react';

// Instruments
import directLogo from '../theme/assets/logos/direct_office-logo.png';
import findAgentLogo from '../theme/assets/logos/find_agent-footer-logo.png';
import odobryLogo from '../theme/assets/logos/odobry-footer-logo.png';
import appStoreLogo from '../theme/assets/logos/appstore.png';
import appGoogleLogo from '../theme/assets/logos/appgoogle.png';

// Components
import Button from './Button';

export default class Footer extends React.Component {
    render() {
        return (
            <div className = 'footer'>
                <div className = 'cont'>
                    <div className = 'g-container'>
                        <div className="g-line"></div>
                        <div className = 'logo'>
                            <div className = 'logo__item'>
                                <a href="" className = 'logo__link'  target="_blank" rel="nofollow noopener">
                                    <img className = 'logo__ico' src={directLogo} alt=""/>
                                </a>
                            </div>
                            <div className = 'logo__item'>
                                <a href="" className = 'logo__link'  target="_blank" rel="nofollow noopener">
                                    <img className = 'logo__ico' src={findAgentLogo} alt=""/>
                                </a>
                            </div>
                            <div className = 'logo__item'>
                                <a href="" className = 'logo__link'  target="_blank" rel="nofollow noopener">
                                    <img className = 'logo__ico' src={odobryLogo} alt=""/>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div className = 'quick'>
                        <div className = 'g-container'>
                            <div className = 'quick__in'>
                                <div className="quick__title">
                                    <p className="quick__txt">
                                        Рекламируй с Reagent и быстро найдешь покупателей
                                    </p>
                                </div>
                                <div className="quick__btn">
                                    <Button mod = "g-btn_big g-btn_w_31 g-btn_bg_green" title = "Начать" />
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className = 'bottom'>
                        <div className = 'g-container'>
                            <div className = 'bottom__cont'>
                                <div className="copyright">
                                    <p className="copyright__txt footer__txt">
                                        © 2015–2018 Reagent.pro <br/>
                                        ООО «Реагент.про». Все права защищены. <br/>
                                        info@reagent.pro
                                    </p>
                                </div>
                                <div className="store">
                                    <div className="store__title">
                                        <div className="store__title__txt footer__txt">для агентов</div>
                                    </div>
                                    <div className="store__list">
                                        <div className="store__item">
                                            <a href="https://itunes.apple.com/app" className="store__link" target="_blank" rel="nofollow noopener">
                                                <img className = 'store__ico' src={appStoreLogo} alt=""/>
                                            </a>
                                        </div>
                                        <div className="store__item">
                                            <a href="https://play.google.com/store/apps" className="store__link" target="_blank" rel="nofollow noopener">
                                                <img className = 'store__ico' src={appGoogleLogo} alt=""/>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}