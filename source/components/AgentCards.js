// Core
import React from 'react';

// Instrument
import agents from '../theme/assets/agents';

// Components
import AgentCard from './AgentCard';

export default class AgentCards extends React.Component {
    render() {
        const agentsJSX = agents.map((agent) => {
                const myImg = require("../theme/assets/agent/agent__ico.jpg");
                return <AgentCard key={agent.id} num={agent.id} status={agent.status} ico={myImg} name={agent.name} feature={agent.feature} rating_val={agent.rating_val} comment_val={agent.comment_val} />
            });
        return (
            <div className = 'agents'>
                <div className = 'g-container'>
                    <ul className="agents__list" >
                        {agentsJSX}
                    </ul>
                </div>
            </div>
        );
    }
}