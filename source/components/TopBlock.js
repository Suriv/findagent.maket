// Core
import React, { Component } from 'react';

// Instrument

// Components
import Navigation from '../components/Navigation';
import Filter from '../components/Filter';

export default (props) => {
    var fil = (props.filter) ? (<Filter />) : ''
    return (
        <div className = { "topCont " + props.mod }>
            <div className = 'g-container'>
                <div className= "content" >
                    <div className="text-wrap">
                        <div className="title">{ props.title }</div>
                        <div className="desc">{ props.desc }</div>
                    </div>
                </div>
            </div>
            <Navigation />
            { fil }
            <div className = 'diz' style= {{ backgroundImage: `url( ${ props.bg } )`}} />
        </div>
    )
}