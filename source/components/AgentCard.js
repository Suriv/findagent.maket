// Core
import React from 'react';
import { Link } from 'react-router-dom';

// Instrument
import starIco from "../theme/assets/sprite/star_empty.svg";
import commentIco from "../theme/assets/sprite/comment.svg";

// Components
import Button from './Button';

export default (props) => {
    const pic ={
      backgroundImage: `url( ${props.ico} )`
    };
    return (
        <li id = { props.num } className="agent-card">
            <div className="agent-card__wrap">
                <div className="agent-card__img" style={pic}>
                </div>
                <div className="agent-card__status g-green">{ props.status }</div>
                <div className="agent-card__name">{props.name}</div>
                <div className="agent-card__feature">{props.feature}</div>
                <div className="agent-card__follow">
                    <Link to={`/agents/${ props.num }`} className = "g-btn g-btn_sm g-btn_brd g-btn_blue">
                        <span className = 'g-btn__txt'>Follow</span>
                    </Link>
                </div>
                <div className="agent-card__info">
                    <div className="agent-card__info-item">
                        <div className="agent-card__rating">
                            <div className="agent-card__info-el star">
                                <img className="agent-card__info-ico" src={starIco} alt=""/>
                                <div className="agent-card__info-val">{props.rating_val}</div>
                            </div>
                            <div className="agent-card__info-title">Рейтинг</div>
                        </div>
                    </div>
                    <div className="agent-card__info-item">
                        <div className="agent-card__comments">
                            <div className="agent-card__info-el comment">
                                <img className="agent-card__info-ico" src={commentIco} alt=""/>
                                <div className="agent-card__info-val">{props.comment_val}</div>
                            </div>
                            <div className="agent-card__info-title">Отзывы</div>
                        </div>
                    </div>
                </div>
                <div className="agent-card__diz" />
            </div>
        </li>
    );
}