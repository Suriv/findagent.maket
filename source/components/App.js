import React, { Fragment } from 'react';

// Home
import Header from '../components/Header';
import Footer from '../components/Footer';

const App = ({ children }) => (
    <Fragment>
        <Header />
            {children}
        <Footer />
    </Fragment>
);

export default App;