// Core
import React from 'react';

// Instrument
import ico_positive from '../theme/assets/icons/feedback_positive.png';
import ico_negative from '../theme/assets/icons/feedback_negative.png';
import ico_arrow_down from '../theme/assets/icons/arrow_down.svg';

export default (props) => {
    const ico_src = (props.status_ico == "positive") ? (ico_positive) : (ico_negative);
    return  (
        <li className="comment__el">
            <div className="comment__ico-cont">
                <div className="comment__ico" style={{backgroundImage: `url(${ props.ico })`}} />
            </div>
            <div className="comment__text">
                <div className="comment__name">{ props.name }</div>
                <div className="comment__date">{ props.date }</div>
                <button className="comment__ctrl">
                    <img className="comment__ctrl-ico" src={ ico_arrow_down } alt="ico" role="presentation" />
                 </button>
                <div className="comment__status">
                    <img className="comment__status-ico" src={ico_src} alt="ico" role="presentation" />
                    <div className="comment__status-who">{ props.status_who }</div>
                </div>
                <div className="comment__desc">{ props.desc }</div>
            </div>
        </li>
    );
};