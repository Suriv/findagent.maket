// Core
import React from 'react';

export default class Filter extends React.Component {
    render() {
        return (
            <div className = 'filter'>
                <div className = 'g-container'>
                    <ul className="filter__list">
                        <li className="filter__item active">
                            <a href="" className="filter__link">
                                <span className="filter__txt">По рейтингу</span>
                            </a>
                        </li>
                        <li className="filter__item">
                            <a href="" className="filter__link">
                                <span className="filter__txt">По популярности</span>
                            </a>
                        </li>
                        <li className="filter__item">
                            <a href="" className="filter__link">
                                <span className="filter__txt">Онлайн</span>
                            </a>
                        </li>
                        <li className="filter__item">
                            <a href="" className="filter__link">
                                <span className="filter__txt">Оставить заявку на покупку</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}