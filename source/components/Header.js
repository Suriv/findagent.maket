// Core
import React from 'react';
import { Link } from 'react-router-dom';

// import styles from './components/header.css';

export default class Header extends React.Component {
    render() {
        return (
            <div className = 'header'>
                <div className = 'g-container'>
                    <div className = 'cont'>
                        <Link to="/" className = 'logo' />
                        <ul className = "header-nav">
                            <li className = 'item'>
                                <a href="" className = 'item__name g-vac'>
                                    <span className = 'item__name-txt g-vac__el'>Запросить выписку</span>
                                </a>
                            </li>
                            <li className = 'item'>
                                <Link to="/user" className = 'item__name g-vac'>
                                    <span className = 'item__name-txt g-vac__el'>Вход</span>
                                </Link>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        );
    }
};