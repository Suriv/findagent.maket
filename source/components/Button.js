// Core
import React from 'react';

export default (props) => {
    return  (
        <button type="button" className = {"g-btn " + props.mod}>
            <span className="g-btn__txt">{ props.title }</span>
        </button>
    );
};