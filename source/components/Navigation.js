// Core
import React, { Component }  from 'react';
import { NavLink } from 'react-router-dom';

export default class Navigation extends Component {
    render() {
        return (
            <div className = 'nav'>
                <div className = 'g-container'>
                    <ul className="nav__list">
                        <li className="nav__item">
                            <NavLink exact to='/agents' className="nav__link">
                                <span className="nav__txt">Рейтинг агентов</span>
                            </NavLink>
                        </li>
                        <li className="nav__item">
                            <NavLink exact to='/insurance' className="nav__link">
                                <span className="nav__txt">Цифровая ипотека</span>
                            </NavLink>
                        </li>
                        <li className="nav__item">
                            <NavLink exact to='/blogs' className="nav__link">
                                <span className="nav__txt">Блоги</span>
                            </NavLink>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}