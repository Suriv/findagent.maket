// Core
import React from 'react';

export default class Feature extends React.Component {
    render() {
        return (
            <div className = 'feature'>
                <div className = 'g-container'>
                    <ul className="feature__list">
                        <li className="feature__item">
                            <div className="feature__item-wrap">
                                <span className="feature__txt">1000+</span>
                                <span className="feature__title">Агентов по недвижимости</span>
                            </div>
                        </li>
                        <li className="feature__item">
                            <div className="feature__item-wrap">
                                <span className="feature__txt">300+</span>
                                <span className="feature__title">Объектов продано</span>
                            </div>
                        </li>
                        <li className="feature__item">
                            <div className="feature__item-wrap">
                                <span className="feature__txt">50 дней</span>
                                <span className="feature__title">Средний срок продажи</span>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}