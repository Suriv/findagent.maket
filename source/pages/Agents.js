// Core
import React from 'react';

// Instrument
import TopBlockDizSrc from "../theme/assets/topBlock/topBlockBg_agents.jpg";

// Components
import TopBlock from '../components/TopBlock';
import AgentCards from '../components/AgentCards';

export default class Findagent extends React.Component {
    render(){
        return(
            <>
                <TopBlock
                    mod = { 'topCont_mbottom topCont_mid' }
                    title = { 'Рейтинг агентов ' }
                    desc = 'Продавать и покупать лучше через проффесиональнов.  '
                    bg = { TopBlockDizSrc }
                    filter = { true }
                />
                <AgentCards />
            </>
        );
    }
};
