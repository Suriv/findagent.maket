// Core
import React from 'react';

// Instrument
import TopBlockDizSrc from "../theme/assets/topBlock/topBlockBg_blogs.jpg";

// Components
import TopBlock from '../components/TopBlock';

export default class Findagent extends React.Component {
    render(){
        return(
            <>
                <TopBlock
                    mod = { 'topCont_mbottom topCont_mid' }
                    title = { '404' }
                    bg = { TopBlockDizSrc }
                    filter = { false }
                />
            </>
        );
    }
};
