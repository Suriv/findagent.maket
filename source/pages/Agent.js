// Core
import React from 'react';

// Instrument
import comments from '../theme/assets/comments';
import blogs from '../theme/assets/blogs';

// Components
import Button from '../components/Button';
import Comment from '../components/Comment';
import Blog from '../components/Blog';

import AgentPageBg from '../theme/assets/agent/agent-p__bg.jpg';
import AgentPageImg from '../theme/assets/agent/agent__ico.jpg';
import AgentSocInst_d from '../theme/assets/icons/instagram_disable.png';
import AgentSocF_d from '../theme/assets/icons/facebook_disable.png';
import AgentSocVk_a from '../theme/assets/icons/vk_active.png';

export default class Findagent extends React.Component {
    state = {
        list: 'comments',
    }

    handleClick = (el) => {
        let parent = el.target.parentNode;
        let param = parent.getAttribute("data-list");
        let parentChildren = parent.parentNode.children;
        [].forEach.call(parentChildren, (item) => { item.classList.remove("active")} );
        parent.classList.add("active");
        this.setState( ({ list }) => ({
            list: param,
        }))
    }

    render(){
        const com = comments.map( (item) => {
                        return (
                            <Comment
                                key={item.id}
                                ico_src={item.ico}
                                ico={`${AgentPageImg}`}
                                date={item.date}
                                status_who={item.status_who}
                                status_ico={item.status_ico}
                                name={item.name}
                                desc={item.desc}
                            />
                        )
                    })
        const bl = blogs.map( (item) => {
                        return (
                            <Blog
                                key={item.id}
                                imgBlog={item.img}
                                viewBlog={item.view}
                                dateBlog={item.date}
                                titleBlog={item.title}
                                descBlog={item.desc}
                            />
                        )
                    })
        const listView = ( this.state.list == 'comments' ) ? (
                                <>
                                    <div className="agent-p__button-b">
                                        <div className="g-container_sm">
                                            <div className="agent-p__button-b-in">
                                                <Button mod="g-btn_bg_grey" title="Написать отзыв" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="agent-p__bottom">
                                        <div className="g-container_sm">
                                            <ul className="comment__list">{com}</ul>
                                            <div className="agent-p__bottom-btn">
                                                <Button mod="g-btn_brd_blue g-btn_mid g-btn_light" title="Показать еще" />
                                            </div>
                                        </div>
                                    </div>
                                </>
                            ) : (
                                <>
                                    <div className="agent-p__bottom">
                                        <div className="g-container_sm">
                                            <ul className="blog__list">{bl}</ul>
                                            <div className="agent-p__bottom-btn">
                                                <Button mod="g-btn_brd_blue g-btn_mid g-btn_light" title="Показать еще" />
                                            </div>
                                        </div>
                                    </div>
                                </>
                            )
        return(
            <>
                <div className="agent-p">
                    <div className="agent-p__bg" style={{ backgroundImage: `url(${AgentPageBg})` }}></div>
                    <div className="agent-p__top">
                        <div className="g-container_sm">
                            <div className="agent-p__wrap">
                                <div className="agent-p__left"></div>
                                <div className="agent-p__right">
                                    <div className="agent-p__flex">
                                        <div className="agent-p__name-cont">
                                            <div className="agent-p__name-wrap">
                                                <div className="agent-p__name">Наталья Петрова</div>
                                            </div>
                                            <div className="agent-p__star">19</div>
                                        </div>
                                        <div className="agent-p__status">Онлайн</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="agent-p__center">
                        <div className="g-container_sm">
                            <div className="agent-p__wrap">
                                <div className="agent-p__left">
                                    <div className="agent-p__info-img" style={{backgroundImage: `url(${AgentPageImg})`}}></div>
                                    <div className="agent-p__info-cont agent-p__info-cont_sm">

                                        <div className="agent-p__left-btn">
                                            <Button mod="g-btn_bg_violet g-btn_sm" title="Написать сообщение" />
                                        </div>

                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name agent-p__info-name_sm">Возраст:</div>
                                            <div className="agent-p__info-val agent-p__info-val_sm">28 лет</div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name agent-p__info-name_sm">Город:</div>
                                            <div className="agent-p__info-val agent-p__info-val_sm">Сочи</div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name agent-p__info-name_sm">Агенство:</div>
                                            <div className="agent-p__info-val agent-p__info-val_sm">Брокер Недвижимости</div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name agent-p__info-name_sm">Опыт работы:</div>
                                            <div className="agent-p__info-val agent-p__info-val_sm">5 лет</div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name agent-p__info-name_sm">Дата регистрации:</div>
                                            <div className="agent-p__info-val agent-p__info-val_sm">Февраль 2018</div>
                                        </div>
                                        <div className="agent-p__left-footer">
                                            <div className="agent-p__info-item">
                                                <div className="agent-p__info-name agent-p__info-name_big">Подписчиков:</div>
                                                <div className="agent-p__info-val agent-p__info-val_big">87</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="agent-p__right">
                                    <div className="agent-p__info-cont agent-p__info-cont_big agent-p__info-cont_nav">
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name">Номер телефона:</div>
                                            <div className="agent-p__info-val">8-918-333-22-33</div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name">Веб-сайт:</div>
                                            <div className="agent-p__info-val">www.petrova_realagent.ru</div>
                                        </div>
                                        <div className="agent-p__info-soc">
                                            <div className="soc-item__cont">
                                                <div className="soc-item__list">
                                                    <div className="soc-item">
                                                        <div className="soc-item__in">
                                                            <img className="soc-item__ico" src={AgentSocF_d} />
                                                        </div>
                                                    </div>
                                                    <div className="soc-item">
                                                        <div className="soc-item__in">
                                                            <img className="soc-item__ico" src={AgentSocInst_d} />
                                                        </div>
                                                    </div>
                                                    <div className="soc-item">
                                                        <a href="https://vk.com/reagentpro" className="soc-item__in" rel="nofollow noopener" target="_blank" >
                                                            <img className="soc-item__ico" src={AgentSocVk_a} />
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name">Интересы:</div>
                                            <div className="agent-p__info-val">Собаки, книги и туфли</div>
                                        </div>
                                        <div className="agent-p__info-item">
                                            <div className="agent-p__info-name">О себе:</div>
                                            <div className="agent-p__info-val">
                                                Я могу воплолить в реальность ваши мечты о жилье.
                                                Опыт работы в этой сфете превышает 5 лет! <br/>
                                                Моя работа - моя страсть!
                                            </div>
                                        </div>
                                        <div className="agent-p__info-nav">
                                            <button className="agent-p__info-nav-item active" data-list = 'comments' onClick={ this.handleClick }>
                                                <span className="agent-p__info-nav-item-txt">Отзывы</span>
                                            </button>
                                            <button className="agent-p__info-nav-item" data-list = 'blogs' onClick={ this.handleClick }>
                                                <span className="agent-p__info-nav-item-txt">Блог</span>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    { listView }
                </div>
            </>
        );
    }
};
