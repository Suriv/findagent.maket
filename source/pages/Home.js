// Core
import React from 'react';

// Instrument
import TopBlockDizSrc from "../theme/assets/topBlock/topBlockBg_home.jpg";

// Components
import TopBlock from '../components/TopBlock';
import LineGreen from '../components/LineGreen';
import Feature from '../components/Feature';
import Homedopnav from '../components/Homedopnav';

export default class Findagent extends React.Component {
    render(){
        return(
            <>
                <TopBlock
                    mod = { ' topCont_big' }
                    title = { 'Оцените спрос на вашу недвижимость' }
                    desc = { 'Наша система покажет, какое количество реальных покупателей есть на ваш объект недвижимости прямо сейчас.' }
                    bg = { TopBlockDizSrc }
                    filter = { false }
                />
                <LineGreen />
                <Feature />
                <Homedopnav />
            </>
        );
    }
};
