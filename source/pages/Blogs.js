// Core
import React from 'react';

// Instrument
import blogs from '../theme/assets/blogs';
import TopBlockDizSrc from "../theme/assets/topBlock/topBlockBg_blogs.jpg";

// Components
import TopBlock from '../components/TopBlock';
import Blog from '../components/Blog';
import Button from '../components/Button';

export default class Findagent extends React.Component {
    render(){
        const blogsJSX = blogs.map((blog) => {
                return <Blog key={blog.id} imgBlog={blog.img} viewBlog={blog.view}  dateBlog={blog.date} titleBlog={blog.title} descBlog={blog.desc} />;
            });
        return(
            <>
                <TopBlock
                    mod = { 'topCont_mbottom topCont_mid' }
                    title = { 'Блоги' }
                    bg = { TopBlockDizSrc }
                    filter = { false }
                />

                <div className="g-container_sm">
                    <div className="blog__list-cont">
                        <ul className="blog__list">
                            { blogsJSX }
                        </ul>
                    </div>
                    <div className="g-pag-btn">
                        <Button mod="g-btn_brd_blue g-btn_mid g-btn_light" title="Показать еще" />
                    </div>
                </div>
            </>
        );
    }
};
