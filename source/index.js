// Core
import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ScrollToTop  from './pages/ScrollToTop';

// Theme
import './theme/init';

import Home from './pages/Home';
import Agents from './pages/Agents';
import Agent from './pages/Agent';
import Insurance from './pages/Insurance';
import Blogs from './pages/Blogs';
import Error from './pages/Error';

ReactDOM.render((
    <BrowserRouter>
        <ScrollToTop>
            <App>
              <Switch>
                <Route exact path="/" component={ Home } />
                <Route exact path="/agents" component={ Agents } />
                <Route path="/agents/:id" component={ Agent } />
                <Route path="/insurance" component={ Insurance } />
                <Route path="/blogs" component={ Blogs } />
                <Route path="*" component={ Error } />
              </Switch>
            </App>
        </ScrollToTop>
    </BrowserRouter>
), document.getElementById('app'));
